<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin(
	$_EXTKEY,
	'Share',
	array(
		'Media' => 'list',
		'Parameter' => '',
		
	),
	// non-cacheable actions
	array(
		'Media' => '',
		'Parameter' => '',
		
	)
);

?>