<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Share',
	'Simply Share'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Simply Share');

t3lib_extMgm::addLLrefForTCAdescr('tx_simplyshare_domain_model_media', 'EXT:simply_share/Resources/Private/Language/locallang_csh_tx_simplyshare_domain_model_media.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_simplyshare_domain_model_media');
$TCA['tx_simplyshare_domain_model_media'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:simply_share/Resources/Private/Language/locallang_db.xml:tx_simplyshare_domain_model_media',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,share_url,icon,account,params,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Media.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/arrow_refresh.png'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_simplyshare_domain_model_parameter', 'EXT:simply_share/Resources/Private/Language/locallang_csh_tx_simplyshare_domain_model_parameter.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_simplyshare_domain_model_parameter');
$TCA['tx_simplyshare_domain_model_parameter'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:simply_share/Resources/Private/Language/locallang_db.xml:tx_simplyshare_domain_model_parameter',
		'label' => 'parameter',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'hideTable' => TRUE,
		'searchFields' => 'parameter,fill_with,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Parameter.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/brick_link.png'
	),
);

?>