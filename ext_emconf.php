<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Simply Share',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Henjo Hoeksma',
	'author_email' => 'hphoeksma@stylence.nl',
	'author_company' => 'Stylence',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.1.0',
	'constraints' => array(
		'depends' => array(
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.5',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>