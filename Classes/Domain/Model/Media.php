<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package simply_share
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_SimplyShare_Domain_Model_Media extends Tx_Extbase_DomainObject_AbstractEntity {

	/**
	 * Media name
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $name;

	/**
	 * Share URI
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $shareUrl;

	/**
	 * Icon
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $icon;

	/**
	 * Account name
	 *
	 * @var string
	 */
	protected $account;

	/**
	 * Extra parameters
	 *
	 * @var Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyShare_Domain_Model_Parameter>
	 * @lazy
	 */
	protected $params;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->params = new Tx_Extbase_Persistence_ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the shareUrl
	 *
	 * @return string $shareUrl
	 */
	public function getShareUrl() {
		return $this->shareUrl;
	}

	/**
	 * Sets the shareUrl
	 *
	 * @param string $shareUrl
	 * @return void
	 */
	public function setShareUrl($shareUrl) {
		$this->shareUrl = $shareUrl;
	}

	/**
	 * Returns the icon
	 *
	 * @return string $icon
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Sets the icon
	 *
	 * @param string $icon
	 * @return void
	 */
	public function setIcon($icon) {
		$this->icon = $icon;
	}

	/**
	 * Returns the account
	 *
	 * @return string $account
	 */
	public function getAccount() {
		return $this->account;
	}

	/**
	 * Sets the account
	 *
	 * @param string $account
	 * @return void
	 */
	public function setAccount($account) {
		$this->account = $account;
	}

	/**
	 * Adds a Parameter
	 *
	 * @param Tx_SimplyShare_Domain_Model_Parameter $param
	 * @return void
	 */
	public function addParam(Tx_SimplyShare_Domain_Model_Parameter $param) {
		$this->params->attach($param);
	}

	/**
	 * Removes a Parameter
	 *
	 * @param Tx_SimplyShare_Domain_Model_Parameter $paramToRemove The Parameter to be removed
	 * @return void
	 */
	public function removeParam(Tx_SimplyShare_Domain_Model_Parameter $paramToRemove) {
		$this->params->detach($paramToRemove);
	}

	/**
	 * Returns the params
	 *
	 * @return Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyShare_Domain_Model_Parameter> $params
	 */
	public function getParams() {
		return $this->params;
	}

	/**
	 * Sets the params
	 *
	 * @param Tx_Extbase_Persistence_ObjectStorage<Tx_SimplyShare_Domain_Model_Parameter> $params
	 * @return void
	 */
	public function setParams(Tx_Extbase_Persistence_ObjectStorage $params) {
		$this->params = $params;
	}

}
?>