<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package simply_share
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_SimplyShare_Controller_MediaController extends Tx_Extbase_MVC_Controller_ActionController {

	/**
	 * mediaRepository
	 *
	 * @var Tx_SimplyShare_Domain_Repository_MediaRepository
	 */
	protected $mediaRepository;

	/**
	 * injectMediaRepository
	 *
	 * @param Tx_SimplyShare_Domain_Repository_MediaRepository $mediaRepository
	 * @return void
	 */
	public function injectMediaRepository(Tx_SimplyShare_Domain_Repository_MediaRepository $mediaRepository) {
		$this->mediaRepository = $mediaRepository;
	}

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$medias = $this->mediaRepository->findAll();
		if(count($medias) === 0) {
			$this->flashMessageContainer->add('No media found to share.', '', t3lib_FlashMessage::ERROR);
		}
		$requestUri = t3lib_div::getIndpEnv('TYPO3_REQUEST_URL');
		$this->view->assign('requestUri', t3lib_div::rawUrlEncodeJS($requestUri));
		$this->view->assign('pageTitle', $GLOBALS['TSFE']->page['title']);
		$this->view->assign('medias', $medias);
	}

}
?>