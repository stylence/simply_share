<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Henjo Hoeksma <hphoeksma@stylence.nl>, Stylence
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_SimplyShare_Domain_Model_Media.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Simply Share
 *
 * @author Henjo Hoeksma <hphoeksma@stylence.nl>
 */
class Tx_SimplyShare_Domain_Model_MediaTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_SimplyShare_Domain_Model_Media
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_SimplyShare_Domain_Model_Media();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setNameForStringSetsName() { 
		$this->fixture->setName('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getName()
		);
	}
	
	/**
	 * @test
	 */
	public function getShareUrlReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setShareUrlForStringSetsShareUrl() { 
		$this->fixture->setShareUrl('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getShareUrl()
		);
	}
	
	/**
	 * @test
	 */
	public function getIconReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setIconForStringSetsIcon() { 
		$this->fixture->setIcon('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getIcon()
		);
	}
	
	/**
	 * @test
	 */
	public function getAccountReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setAccountForStringSetsAccount() { 
		$this->fixture->setAccount('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getAccount()
		);
	}
	
	/**
	 * @test
	 */
	public function getParamsReturnsInitialValueForObjectStorageContainingTx_SimplyShare_Domain_Model_Parameter() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getParams()
		);
	}

	/**
	 * @test
	 */
	public function setParamsForObjectStorageContainingTx_SimplyShare_Domain_Model_ParameterSetsParams() { 
		$param = new Tx_SimplyShare_Domain_Model_Parameter();
		$objectStorageHoldingExactlyOneParams = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneParams->attach($param);
		$this->fixture->setParams($objectStorageHoldingExactlyOneParams);

		$this->assertSame(
			$objectStorageHoldingExactlyOneParams,
			$this->fixture->getParams()
		);
	}
	
	/**
	 * @test
	 */
	public function addParamToObjectStorageHoldingParams() {
		$param = new Tx_SimplyShare_Domain_Model_Parameter();
		$objectStorageHoldingExactlyOneParam = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneParam->attach($param);
		$this->fixture->addParam($param);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneParam,
			$this->fixture->getParams()
		);
	}

	/**
	 * @test
	 */
	public function removeParamFromObjectStorageHoldingParams() {
		$param = new Tx_SimplyShare_Domain_Model_Parameter();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($param);
		$localObjectStorage->detach($param);
		$this->fixture->addParam($param);
		$this->fixture->removeParam($param);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getParams()
		);
	}
	
}
?>